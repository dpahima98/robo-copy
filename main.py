from time import time, sleep
import os
import shutil

def isNull(var, name):
    if var is None:
        print("{0} is missing , This variable is required!".format(name))
        exit(1)

directoryPath = os.environ.get("DIRECTORY_PATH")
isNull(directoryPath, name="DIRECTORY_PATH")

remotePath = os.environ.get("REMOTE_PATH")
isNull(remotePath, name="REMOTE_PATH")

while True:
    if len(os.listdir(directoryPath)) == 0:
        print("No files found in the directory.")
    else:
        print("***Some files found in the directory***")
        imagePath = directoryPath + os.listdir(directoryPath)[0]
        shutil.copy(imagePath, remotePath)
        print("*** FIND - {0} - ***".format(os.listdir(directoryPath)[0]))
        os.remove(imagePath)
        print("*** REMOVED ***")

    sleep(60 - time() % 60)